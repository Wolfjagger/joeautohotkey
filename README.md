joeautohotkey


Installation
===

Install AutoHotkey

Create shortcuts for each desired AutoHotkey script and copy to the startup folder
 (Win+r->shell:startup).

Install win-10-virtual-desktop-enhancer (https://github.com/sdias/win-10-virtual-desktop-enhancer.git).
 Place the .ahk into the startup folder.

Place settings.ini into the base of the win-10 folder (or update existing).

Install latest VS redistributable (>2015v3).
